import 'package:flutter/material.dart';
import 'package:shop_with_state_management/models/product.dart';
import 'package:shop_with_state_management/widgets/product_item.dart';

class ProductOverviewScreen extends StatelessWidget {
  final List<Product> productLoader = [
    Product(
      id: "p1",
      title: "Product1",
      description: "that\'s nice",
      price: 10.0,
      imageUrl:
          "https://images.pexels.com/photos/90946/pexels-photo-90946.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
    ),
    Product(
      id: "p2",
      title: "Product2",
      description: "love it",
      price: 15.0,
      imageUrl:
          "https://www.redken.com/~/media/redken-commerce/images/styling/volume-maximizer/redken-2020-volume-maximizer-shampoo-product-shot-1260x1600.jpg?rz=1&rh=290&rw=230&hash=F8D1F9576B2910032C6932E282C79BBD95126F61",
    ),
    Product(
      id: "p3",
      title: "Product3",
      description: "good show avaliable",
      price: 20.0,
      imageUrl:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTy3my-rKzuSmcdkIOQ4JwbJ1MsbQ_C8AZSCUl2HAyhUK6DiY2GXJP3xfDMM0k1vUMLmRA&usqp=CAU",
    ),
    Product(
      id: "p4",
      title: "Product4",
      description: "that\'s nice",
      price: 7.0,
      imageUrl:
          "https://img.pixelz.com/blog/using-product-images-on-ecommerce-site/F_Purse1_center.jpg?w=1000",
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Shop"),
      ),
      body: GridView.builder(
        padding: EdgeInsets.all(10),
        itemCount: productLoader.length,
        itemBuilder: (ctx, index) => ProductItem(productLoader[index].id,
            productLoader[index].title, productLoader[index].imageUrl),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 10,
            mainAxisSpacing: 10),
      ),
    );
  }
}
